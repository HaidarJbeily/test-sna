# test_app.py
import unittest
from app import hello

class TestApp(unittest.TestCase):

    def test_hello_endpoint(self):
        self.assertEqual(hello(), 'Hello, World!')

if __name__ == '__main__':
    unittest.main()
